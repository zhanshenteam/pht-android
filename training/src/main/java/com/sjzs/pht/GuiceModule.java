package com.sjzs.pht;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

/**
 * Created by nielongyu on 16/3/12.
 */
public class GuiceModule extends AbstractModule {
    private Application application;

    public GuiceModule(Application application) {
        this.application = application;
    }

    @Override
    protected void configure() {

    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new GsonBuilder().
                serializeNulls().
                create();
    }
}
