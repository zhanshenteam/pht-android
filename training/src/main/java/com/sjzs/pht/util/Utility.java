package com.sjzs.pht.util;

import android.content.Context;

/**
 * Created by nielongyu on 16/1/14.
 */
public class Utility {
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
