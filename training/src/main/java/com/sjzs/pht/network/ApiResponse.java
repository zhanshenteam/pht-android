package com.sjzs.pht.network;

import android.support.annotation.NonNull;
import android.text.TextUtils;

/**
 * Created by nielongyu on 16/1/14.
 */
@SuppressWarnings("unused")
@NoProGuard
public class ApiResponse<T> {
    private T data = null;
    private errorMsg errorMsg = null;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public errorMsg getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(errorMsg errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isSuccess() {
        return null == errorMsg && null != data;
    }

    public String getErrorMsg(@NonNull String defaultMsg) {
        if (null != errorMsg && !TextUtils.isEmpty(errorMsg.getMessage())) {
            return errorMsg.getMessage();
        } else {
            return defaultMsg;
        }
    }

    public int getErrorCode() {
        if (null != errorMsg) {
            return errorMsg.getCode();
        } else {
            return 0;
        }
    }

    public static class errorMsg {
        private int code;
        private String type;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class Page {
        private int pageNo;
        private int pageSize;
        private int totalCount;
        private int totalPageCount;

        public int getPageNo() {
            return pageNo;
        }

        public void setPageNo(int pageNo) {
            this.pageNo = pageNo;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public int getTotalPageCount() {
            return totalPageCount;
        }

        public void setTotalPageCount(int totalPageCount) {
            this.totalPageCount = totalPageCount;
        }
    }
}