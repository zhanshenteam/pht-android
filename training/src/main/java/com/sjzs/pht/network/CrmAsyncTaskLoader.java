package com.sjzs.pht.network;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import java.net.ProtocolException;
import java.net.UnknownHostException;

import retrofit.Call;
import retrofit.Response;

/**
 * Created by nielongyu on 16/1/14.
 */
public abstract class CrmAsyncTaskLoader<T> extends AsyncTaskLoader<T> {

    protected Context mContext;

    public CrmAsyncTaskLoader(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();
    }

    protected static <T> ApiResponse<T> convert(Response<ApiResponse<T>> apiResponse) throws Exception {
        return apiResponse.body();
    }

    protected static <T> ApiResponse<T> callConvert(Call<ApiResponse<T>> call) {
        try {
            Response<ApiResponse<T>> response = call.execute();
            ApiResponse<T> apiResponse = convert(response);
            if (!apiResponse.isSuccess()) {
                if (null != apiResponse.getErrorMsg()) {
                    ApiExceptionHandler.onBusinessException(apiResponse);
                }
            }
            return apiResponse;
        } catch (ProtocolException e) {
            return new ApiResponse<>();
        } catch (UnknownHostException e) {
            ApiExceptionHandler.onUnKnowHostException();
            return new ApiResponse<>();
        } catch (Exception e) {
            return new ApiResponse<>();
        }
    }
}
