package com.sjzs.pht.network;

import android.content.Context;

import com.squareup.okhttp.Request;

import java.nio.charset.Charset;


/**
 * Created by nielongyu on 16/1/14.
 */
@NoProGuard
public class ApiExceptionHandler {

    private static final int HTTP_OK = 200;
    private static final int RESPONSE_NULL = 1;
    private static final int PARSE_ERROR = 2;
    private static final Charset UTF8 = Charset.forName("UTF-8");
    private static HandlerListener mListener = null;

    /**
     * HandlerListener 执行在非UI线程!!!! 切勿在方法中添加Toast !!
     *
     * @param context 只接受Application的context
     */
    public static void init(final Context context) {
        mListener = new HandlerListener() {
            @Override
            public <T> void onBusinessException(ApiResponse<T> apiResponse) {

            }

            @Override
            public <T> void onConvertException(ApiResponse<T> apiResponse) {

            }

            @Override
            public void onUnKnowHostException() {

            }

            @Override
            public void onHandleResponse(Request request, com.squareup.okhttp.Response apiResponse) {

            }
        };
    }

    public static <T> void onBusinessException(ApiResponse<T> apiResponse) {
        if (mListener == null) {
            return;
        }
        mListener.onBusinessException(apiResponse);
    }

    public static <T> void onConvertException(ApiResponse<T> apiResponse) {
        if (mListener == null) {
            return;
        }
        mListener.onConvertException(apiResponse);
    }

    public static void onHandleResponse(Request request, com.squareup.okhttp.Response apiResponse) {
        if (mListener == null) {
            return;
        }
        mListener.onHandleResponse(request, apiResponse);
    }

    public static void onUnKnowHostException() {
        if (mListener == null) {
            return;
        }
        mListener.onUnKnowHostException();
    }

    public interface HandlerListener {
        <T> void onBusinessException(ApiResponse<T> apiResponse);

        <T> void onConvertException(ApiResponse<T> apiResponse);

        void onUnKnowHostException();

        void onHandleResponse(Request request, com.squareup.okhttp.Response apiResponse);
    }
}
