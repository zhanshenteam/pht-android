package com.sjzs.pht.base;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;

import com.sjzs.pht.R;
import com.sjzs.pht.util.Utility;
import com.umeng.analytics.MobclickAgent;

import roboguice.activity.RoboFragmentActivity;

/**
 * Created by nielongyu on 16/1/14.
 */
@SuppressWarnings("unused")
public class BaseActivity extends RoboFragmentActivity {

    private static final String TAG = "BaseActivity";
    protected volatile SparseBooleanArray loaderInit = new SparseBooleanArray();
    protected BaseActivity instance;
    protected Toolbar mToolbar;
    protected int statusBarHeight = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            statusBarHeight = Utility.getStatusBarHeight(getApplicationContext());
            Log.d(TAG, "onCreate: statusBarHeight" + statusBarHeight);
        }
        instance = this;
    }

    protected boolean needStatusBar() {
        return true;
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        if (needStatusBar()) {
            initStatusBar();
        }
        try {
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
        } catch (Exception e) {
            Log.e(TAG, "setContentView: no toolbar");
        }
    }

    private void initStatusBar() {
        try {
            View statusHeaderView = findViewById(R.id.statusHeaderView);
            statusHeaderView.getLayoutParams().height = statusBarHeight;
        } catch (NullPointerException ignored) {
            Log.d(TAG, "no header view");
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        loaderInit.clear();
    }

    @SuppressWarnings("deprecation")
    public Drawable drawable(int drawableId) {
        return getResources().getDrawable(drawableId);
    }

    /**
     * 封装加载loader，子类只需直接调用即可，不需要区分init还是reload
     *
     * @param callbacks id默认取Callbacks的hashCode
     * @param <T>       返回的model
     */
    protected <T> void startLoader(@NonNull LoaderManager.LoaderCallbacks<T> callbacks) {
        if (loaderInit.get(callbacks.hashCode(), false)) {
            getSupportLoaderManager().restartLoader(callbacks.hashCode(), null, callbacks);
        } else {
            loaderInit.put(callbacks.hashCode(), true);
            getSupportLoaderManager().initLoader(callbacks.hashCode(), null, callbacks);
        }
    }

    protected <T> void startLoader(@NonNull Bundle bundle, @NonNull LoaderManager.LoaderCallbacks<T> callbacks) {
        if (loaderInit.get(callbacks.hashCode(), false)) {
            getSupportLoaderManager().restartLoader(callbacks.hashCode(), bundle, callbacks);
        } else {
            loaderInit.put(callbacks.hashCode(), true);
            getSupportLoaderManager().initLoader(callbacks.hashCode(), bundle, callbacks);
        }
    }

    protected <T> void destroyLoader(@NonNull LoaderManager.LoaderCallbacks<T> callbacks) {
        getSupportLoaderManager().destroyLoader(callbacks.hashCode());
    }

}
