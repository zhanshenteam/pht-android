package com.sjzs.pht.base;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sjzs.pht.R;
import com.sjzs.pht.util.Utility;
import com.umeng.analytics.MobclickAgent;

import roboguice.fragment.RoboFragment;


/**
 * Created by nielongyu on 16/1/14.
 */
@SuppressWarnings("unused")
public abstract class BaseFragment extends RoboFragment {

    private static final String TAG = "BaseFragment";
    protected volatile SparseBooleanArray loaderInit = new SparseBooleanArray();
    protected int statusBarHeight = 0;

    protected abstract String setViewTag();

    protected View createView(LayoutInflater inflater, ViewGroup container, int layoutResId) {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            statusBarHeight = Utility.getStatusBarHeight(getActivity().getApplicationContext());
            Log.d(TAG, "onCreate: statusBarHeight" + statusBarHeight);
        }
        View view = inflater.inflate(layoutResId, container, false);
        if (needStatusBar()) {
            initStatusBar(view);
        }
        return view;
    }

    protected boolean needStatusBar() {
        return false;
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart(setViewTag());
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd(setViewTag());
        loaderInit.clear();
    }

    private void initStatusBar(View view) {
        try {
            View statusHeaderView = view.findViewById(R.id.statusHeaderView);
            statusHeaderView.getLayoutParams().height = statusBarHeight;
        } catch (NullPointerException ignored) {
            Log.d(TAG, "no header view");
        }
    }

    /**
     * 封装加载loader，子类只需直接调用即可，不需要区分init还是reload
     *
     * @param callbacks 默认取callbacks 的hashCode作为id
     * @param <T>       model
     */
    protected <T> void startLoader(@NonNull LoaderManager.LoaderCallbacks<T> callbacks) {
        if (loaderInit.get(callbacks.hashCode(), false)) {
            getLoaderManager().restartLoader(callbacks.hashCode(), null, callbacks);
        } else {
            loaderInit.put(callbacks.hashCode(), true);
            getLoaderManager().initLoader(callbacks.hashCode(), null, callbacks);
        }
    }

    protected <T> void startLoader(@NonNull Bundle bundle, @NonNull LoaderManager.LoaderCallbacks<T> callbacks) {
        if (loaderInit.get(callbacks.hashCode(), false)) {
            getLoaderManager().restartLoader(callbacks.hashCode(), bundle, callbacks);
        } else {
            loaderInit.put(callbacks.hashCode(), true);
            getLoaderManager().initLoader(callbacks.hashCode(), bundle, callbacks);
        }
    }

    protected <T> void destroyLoader(@NonNull LoaderManager.LoaderCallbacks<T> callbacks) {
        getLoaderManager().destroyLoader(callbacks.hashCode());
    }
}
